<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Assignment Operator</title>
    <style>
        .phpcoding{
            width:900px;
            margin: 0 auto;
            background: #dddddd;
        }

        .headerpart, .footerpart{
            background: #444;
            padding:20px;
            color: #fff;
            text-align: center;
        }

        .maincontent{
            min-height: 400px;
            padding:20px;
        }
        .headerpart h2, .footerpart h2{
            margin: 0;
        }

    </style>
</head>

<body>
<div class="phpcoding">

    <section class="headerpart">
        <h2>Assignment Operator</h2>
    </section>

    <section class="maincontent">
        <?php
        $x = 20;
        $y = 85;

        echo $y / $x;
        ?>
    </section>

    <section class="footerpart">
        <h2>Asssignment On day-5, Emran Hosen 137033 B22  </h2>
    </section>
</div>
</body>
</html>